<?php

namespace Sur3s\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sur3s\LoginBundle\Entity\Users;
use Sur3s\LoginBundle\Entity\Images;

class DefaultController extends Controller {

        public function indexAction(Request $request) {
                $session = $this->getRequest()->getSession();
                $em = $this->getDoctrine()->getEntityManager();
                $repository = $em->getRepository('LoginBundle:Users');

                if ($request->getMethod() == 'POST') {
                        $session->clear();

                        $username = $request->get('username');
                        $password = sha1($request->get('password'));
                        $remember = $request->get('remember');


                        $user = $repository->findOneBy(array('userName' => $username, 'password' => $password));

                        if ($user) {
                                if ($remember == 'rem_me') {
                                        $login = new Users();
                                        $login->setUserName($username);
                                        $login->setPassword($password);
                                        $session->set('login', $login);
                                }
                                return $this->render('LoginBundle:Default:home.html.twig', array('name' => $user->getUserName()));
                        } else {
                                return $this->render('LoginBundle:Default:login.html.twig', array('error' => 'Error Login!!!'));
                        }
                } else {
                        if ($session->has('login')) {
                                $login = $session->get('login');
                                $username = $login->getUserName();
                                $password = $login->getPassword();
                                $user = $repository->findOneBy(array('userName' => $username, 'password' => $password));

                                if ($user) {
                                        return $this->render('LoginBundle:Default:home.html.twig', array('name' => $user->getUserName()));
//                                        return $this->redirect($this->generateUrl('login_login_success'));
                                }
                        }
                        return $this->render('LoginBundle:Default:login.html.twig');
                }
        }

        public function signupAction(Request $request) {
                if ($request->getMethod() == 'POST') {
                        $userName = $request->get('username');
                        $firstName = $request->get('firstname');
                        $lastName = $request->get('lastname');
//                        $email = $request->get('email');
                        $password = sha1($request->get('passwd'));


                        $user = new Users();
                        $user->setFirstName($firstName);
                        $user->setLastName($lastName);
                        $user->setUserName($userName);
                        $user->setPassword($password);

                        $em = $this->getDoctrine()->getEntityManager();
                        $em->persist($user);
                        $em->flush();
                }
                return $this->render('LoginBundle:Default:login.html.twig');
        }

        public function logoutAction(Request $request) {
                $session = $this->getRequest()->getSession();
                $session->clear();
                return $this->render('LoginBundle:Default:login.html.twig');
        }

        public function gallaryAction() {
//                        var_dump("This is gallary page...");exit();
//                        $images = $this->getDoctrine();

                return $this->render('LoginBundle:Default:gallary.html.twig', array('name' => 'Admin'));
        }

        public function uploadAction(Request $request) {
                $image = new Images();
                $form = $this->createFormBuilder($image)
                        ->add('name', 'text', array(
                            'label' => 'Give image Name:  ',
//                            'label_attr' => array(
//                                'attr' => array(
//                                    'class'=>'col-lg-2 control-label'
//                                )
//                            ),
                            'attr' => array(
                                'class' => ''
                            )
                        ))
                        ->add('file', 'file', array(
                            'label' => 'Get file location:',
                            'attr' => array(
                                'class' => 'btn btn-default btn-file'
                            )
                        ))
                        ->add('Upload', 'submit', array(
                            'attr' => array(
                                'class' => 'btn btn-sm btn-success'
                            )
                        ))
                        ->getForm();

                $form->handleRequest($request);

                if ($form->isValid()) {
                        $em = $this->getDoctrine()->getManager();

                        $image->upload();

                        $em->persist($image);
                        $em->flush();

                        return $this->redirect($this->generateUrl('login_upload'));
                }
                return $this->render('LoginBundle:Default:upload.html.twig', array('image_form' => $form->createView(), 'name' => 'Admin'));
//                return array('form' => $form->createView());
        }

}
