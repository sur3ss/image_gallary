<?php

namespace Sur3s\LoginBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Images
 *
 * @ORM\Table(name = "images")
 * @ORM\Entity
 */
class Images {

        /**
         * @var integer
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=255)
         * @Assert\NotBlank
         */
        public $name;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        public $path;
        
        /**
         * @ORM\ManyToOne(targetEntity="Users", inversedBy="images")
         * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
         */
        protected $user;
        
        /**
         * @Assert\File(maxSize="6000000")
         */
        private $file;
        private $temp;

        /**
         * Get id
         *
         * @return integer 
         */
        public function getId() {
                return $this->id;
        }


    /**
     * Set name
     *
     * @param string $name
     * @return Images
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Images
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set user
     *
     * @param \Sur3s\LoginBundle\Entity\Users $user
     * @return Images
     */
    public function setUser(\Sur3s\LoginBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Sur3s\LoginBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function getAbsolutePath() {
                return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
        }

        public function getWebPath() {
                return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
        }

        protected function getUploadRootDir() {
                // the absolute directory path where uploaded
                // documents should be saved
//                var_dump(__DIR__ . '/web/' . $this->getUploadDir());exit();
//                return __DIR__ . '/web/' . $this->getUploadDir();
                return '/home/sfprojects/LoginProject/web/'.$this->getUploadDir();
        }

        protected function getUploadDir() {
                // get rid of the __DIR__ so it doesn't screw up
                // when displaying uploaded doc/image in the view.
                return 'uploads/documents';
        }

        /**
         * Sets file.
         *
         * @param UploadedFile $file
         */
        public function setFile(UploadedFile $file = null) {
                $this->file = $file;
                // check if we have an old image path
                if (isset($this->path)) {
                        // store the old name to delete after the update
                        $this->temp = $this->path;
                        $this->path = null;
                } else {
                        $this->path = 'initial';
                }
        }
        
        /**
         * Get file.
         *
         * @return UploadedFile
         */
        public function getFile() {
                return $this->file;
        }

        /**
         * @ORM\PrePersist()
         * @ORM\PreUpdate()
         */
        public function preUpload() {
                if (null !== $this->getFile()) {
                        // do whatever you want to generate a unique name
                        $filename = sha1(uniqid(mt_rand(), true));
                        $this->path = $filename . '.' . $this->getFile()->guessExtension();
                }
        }

        /**
         * @ORM\PostPersist()
         * @ORM\PostUpdate()
         */
        public function upload() {
                if (null === $this->getFile()) {
                        return;
                }

                // if there is an error when moving the file, an exception will
                // be automatically thrown by move(). This will properly prevent
                // the entity from being persisted to the database on error
                $this->getFile()->move(
                        $this->getUploadRootDir(), 
                        $this->getFile()->getClientOriginalName()
                        );
                
                        $this->path = $this->getFile()->getClientOriginalName();
                
                // check if we have an old image
                if (isset($this->temp)) {
                        // delete the old image
                        unlink($this->getUploadRootDir() . '/' . $this->temp);
                        // clear the temp image path
                        $this->temp = null;
                }
                $this->file = null;
        }

        /**
         * @ORM\PostRemove()
         */
        public function removeUpload() {
                if ($file = $this->getAbsolutePath()) {
                        unlink($file);
                }
        }
}
